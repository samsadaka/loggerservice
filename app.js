require('dotenv').config();
const express = require('express');
const app = express();

//Parse body -- JSON
app.use(express.json());

//ROUTES
app.use('', require('./routes/main'));

//Server listen
app.listen(process.env.PORT, console.log(`Server started on port ${process.env.PORT}`));