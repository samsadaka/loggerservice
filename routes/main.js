const express = require('express');
const log = require('../services/logger_service');
const router = express.Router();

router.post('/', log, async (req, res) => {
    try {
        res.send({message: "Done"});
    } catch (error) {
        res.status(500).send(error);
    }
});

module.exports = router;