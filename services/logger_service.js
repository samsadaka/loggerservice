const Shell = require('node-powershell');

const ps = new Shell({
    executionPolicy: 'Bypass',
    noProfile: true
});

// Log Service
const log = (user_name, user_event, log_path) => {
    ps.clear();
    ps.addCommand(`./logscript.ps1 ${log_path} "(${user_name}) ${user_event}"`);
    return ps.invoke();
};

const LoggerService = async (req, res, next) => {
    log(req.body.user_name, req.body.user_event, req.body.log_path).then(output => {
        next();
    }).catch(err => {
        console.log(err);
        res.status(500).send({ error: 'Internal server error. Please try again later.' });
    });
}

module.exports = LoggerService;