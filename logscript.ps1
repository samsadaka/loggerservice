param($logFile, $logString)

if ($null -ne $logFile) {
    $date = Get-Date -UFormat "%m/%d/%Y %r"
    Add-Content $logFile "[$date] $logString"
} else {
    Throw 'Log file path is missing !'
}

<#
function Log {
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true, Position = 0, HelpMessage = "Log file path")]
        [string] $logFile,
        [Parameter(Mandatory = $true, Position = 1, HelpMessage = "Log String")]
        [string] $logString
    )

    $date = Get-Date -UFormat "%m/%d/%Y %r"
    Add-Content $logFile "[$date] $logString"
}
Log #>